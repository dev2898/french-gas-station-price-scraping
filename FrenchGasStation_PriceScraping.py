import requests
import zipfile
import codecs
import os
import lxml
from bs4 import BeautifulSoup

# Path where unzip the donwload file
PATH = '/home/pi/Dev/'

# Directory of my favorite station ["CustomName":"id"]
MY_STATION = {"Total Pont De Bezons": "92000018", "Bp Colombes McDo": "92700023", "Shell Colombes A86": "92700020"}

# Watch https://www.prix-carburants.gouv.fr/rubrique/opendata/
url = ('https://donnees.roulez-eco.fr/opendata/instantane')

# Download and unzip the xml file 
PrixCarburants_instantane_zip = requests.get(url, allow_redirects=True)
open('PrixCarburants_instantane.zip', 'wb').write(PrixCarburants_instantane_zip.content)

with zipfile.ZipFile('PrixCarburants_instantane.zip','r') as zip_ref:
    zip_ref.extractall(PATH)

# Parse the xml file with bs4
# Unfortunately the xml file is large (All french gas station). So a bit long to parse.
# Unfortunately no file by department or region
PrixCarburants_instantane_xml = open('PrixCarburants_instantane.xml', 'r', encoding='ISO-8859-1')
soup = BeautifulSoup(PrixCarburants_instantane_xml, 'lxml')

for i in MY_STATION:
	find_MyStation = soup.find('pdv', {'id':MY_STATION[i]})
	print(' ')
	print('Station ' + i + ' : ')
	for prix in find_MyStation.find_all('prix'):
		# prix is type : bs4.element.Tag
		# Exemple : <prix id="1" maj="2022-09-03 00:35:00" nom="Gazole" valeur="1.863"></prix>
		date = prix.get('maj')
		nom = prix.get('nom')
		valeur = prix.get('valeur')
		print(str(date) + ' '  + str(nom) + ' : ' + str(valeur))

# Remove the zip en xml file previously donwload
os.remove('PrixCarburants_instantane.zip')
os.remove ('PrixCarburants_instantane.xml')
