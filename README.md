# French Gas Station Price Scraping

Get the price of gas in your favorite stations.
Add your favorite fench gas station in MY_STATION dictionary.

For exemple :
```
MY_STATION = {"Total Pont De Bezons": "92000018", "Bp Colombes McDo": "92700023", "Shell Colombes A86": "92700020"}
```

Return :

````
Station Total Pont De Bezons :
2022-09-05 09:00:00 E85 : 0.749
2022-09-05 09:00:00 GPLc : 0.784
2022-09-05 09:00:00 SP98 : 1.558

Station Bp McDo :
2022-09-06 00:01:00 Gazole : 2.059
2022-09-06 00:01:00 E85 : 0.809
2022-09-06 00:01:00 E10 : 1.749
2022-09-06 00:01:00 SP98 : 1.869

Station Shell A86 :
2022-09-03 00:35:00 Gazole : 1.978
2022-09-03 00:35:00 GPLc : 1.036
2022-09-03 00:35:00 E10 : 1.758
2022-09-03 00:35:00 SP98 : 1.878
````
